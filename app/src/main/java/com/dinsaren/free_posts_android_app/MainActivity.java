package com.dinsaren.free_posts_android_app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinsaren.free_posts_android_app.adapters.home.HomeCategoryAdapter;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.app.BaseActivity;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.home.HomeResponse;
import com.dinsaren.free_posts_android_app.models.product.Product;
import com.dinsaren.free_posts_android_app.ui.auth.LoginActivity;
import com.dinsaren.free_posts_android_app.ui.category.CategoryActivity;
import com.dinsaren.free_posts_android_app.ui.product.ProductActivity;
import com.dinsaren.free_posts_android_app.ui.product.ProductDetailActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {
    private Button btnLogin;
    private ProgressBar progressBar;
    private HomeCategoryAdapter homeCategoryAdapter;
    private APIInterface apiInterface;
    private RecyclerView recyclerViewCategory;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setText("Logout");
        initView();
        setVariable();
        getCategory();
    }

    public void initView() {
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setText("Logout");
        recyclerViewCategory = findViewById(R.id.recyclerViewCategory);
        progressBar = findViewById(R.id.progressBar);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
    }

    public void setVariable() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                getCategory();
            }
        });
    }

    public void getCategory() {
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.getAllCategoryWithProduct(UserSharedPreference.getAccessToken(this)).enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    homeCategoryAdapter = new HomeCategoryAdapter(response.body().getData(), MainActivity.this, new HomeCategoryAdapter.OnClickListener() {
                        @Override
                        public void onClickProductImage(Product product) {
                            Intent intent =  new Intent(MainActivity.this, ProductDetailActivity.class);
                            intent.putExtra("ID", product.getId());
                            startActivity(intent);
                        }
                    });
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(MainActivity.this, 1) {
                        @Override
                        public boolean checkLayoutParams(RecyclerView.LayoutParams lp) {
                            lp.width = getWidth() / getSpanCount();
                            return true;
                        }

                        @Override
                        public boolean canScrollVertically() {
                            return true;
                        }
                    };
                    recyclerViewCategory.setLayoutManager(gridLayoutManager);
                    recyclerViewCategory.setAdapter(homeCategoryAdapter);

                }
            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void onOpenListCategory(View view) {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }

    public void onOpenListProduct(View view) {
        Intent intent = new Intent(this, ProductActivity.class);
        startActivity(intent);
    }

    public void onClickOpenLoginActivity(View view) {
        if (!UserSharedPreference.getAccessToken(this).equals("")) {
            UserSharedPreference.removeUser(this);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }

}