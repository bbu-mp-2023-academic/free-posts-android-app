package com.dinsaren.free_posts_android_app.models.register;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("messageKh")
    private String messageKh;
    @SerializedName("messageCh")
    private String messageCh;
    @SerializedName("code")
    private String code;
    @SerializedName("data")
    private String data;

    public RegisterResponse() {
    }

    public RegisterResponse(String message, String messageKh, String messageCh, String code, String data) {
        this.message = message;
        this.messageKh = messageKh;
        this.messageCh = messageCh;
        this.code = code;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageKh() {
        return messageKh;
    }

    public void setMessageKh(String messageKh) {
        this.messageKh = messageKh;
    }

    public String getMessageCh() {
        return messageCh;
    }

    public void setMessageCh(String messageCh) {
        this.messageCh = messageCh;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
