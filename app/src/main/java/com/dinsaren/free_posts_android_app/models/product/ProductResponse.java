package com.dinsaren.free_posts_android_app.models.product;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductResponse{

	@SerializedName("code")
	private String code;

	@SerializedName("data")
	private List<Product> data;

	@SerializedName("message")
	private String message;

	@SerializedName("messageKh")
	private String messageKh;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setData(List<Product> data){
		this.data = data;
	}

	public List<Product> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setMessageKh(String messageKh){
		this.messageKh = messageKh;
	}

	public String getMessageKh(){
		return messageKh;
	}

	@Override
 	public String toString(){
		return 
			"ProductResponse{" + 
			"code = '" + code + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",messageKh = '" + messageKh + '\'' + 
			"}";
		}
}