package com.dinsaren.free_posts_android_app.models;

import com.dinsaren.free_posts_android_app.models.product.Product;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category {

	@SerializedName("nameKh")
	private String nameKh;

	@SerializedName("imageUrl")
	private String imageUrl;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private String status;

	@SerializedName("products")
	private List<Product> products;

	public void setNameKh(String nameKh){
		this.nameKh = nameKh;
	}

	public String getNameKh(){
		return nameKh;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
 	public String toString(){
		return 
			"Category{" +
			"nameKh = '" + nameKh + '\'' + 
			",imageUrl = '" + imageUrl + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}