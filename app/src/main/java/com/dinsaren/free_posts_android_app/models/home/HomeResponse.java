package com.dinsaren.free_posts_android_app.models.home;

import com.dinsaren.free_posts_android_app.models.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("messageKh")
    private String messageKh;
    @SerializedName("messageCh")
    private String messageCh;
    @SerializedName("code")
    private String code;
    @SerializedName("data")
    private List<Category> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageKh() {
        return messageKh;
    }

    public void setMessageKh(String messageKh) {
        this.messageKh = messageKh;
    }

    public String getMessageCh() {
        return messageCh;
    }

    public void setMessageCh(String messageCh) {
        this.messageCh = messageCh;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }
}
