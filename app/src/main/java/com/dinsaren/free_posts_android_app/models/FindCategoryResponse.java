package com.dinsaren.free_posts_android_app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FindCategoryResponse {

	@SerializedName("code")
	private String code;

	@SerializedName("data")
	private Category data;

	@SerializedName("message")
	private String message;

	@SerializedName("messageKh")
	private String messageKh;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setMessageKh(String messageKh){
		this.messageKh = messageKh;
	}

	public String getMessageKh(){
		return messageKh;
	}

	public Category getData() {
		return data;
	}

	public void setData(Category data) {
		this.data = data;
	}

	@Override
 	public String toString(){
		return 
			"CategoryResponse{" + 
			"code = '" + code + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",messageKh = '" + messageKh + '\'' + 
			"}";
		}
}