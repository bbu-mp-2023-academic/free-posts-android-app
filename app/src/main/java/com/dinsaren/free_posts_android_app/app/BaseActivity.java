package com.dinsaren.free_posts_android_app.app;

import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.ui.auth.LoginActivity;
import com.google.android.material.snackbar.Snackbar;

public class BaseActivity extends AppCompatActivity {
    protected ProgressBar progressBar;

    protected void showErrorMessage(String message, View view) {
        Snackbar.make(this, view, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (UserSharedPreference.getAccessToken(this).equals("")) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }
}
