package com.dinsaren.free_posts_android_app.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.dinsaren.free_posts_android_app.models.login.LoginResponse;

public class UserSharedPreference {
    private static SharedPreferences sharedPreferences;
    private static String USER_DATA = "USER_DATA";
    private static String username = "USER_NAME";
    private static String accessToken = "ACCESS_TOKEN";


    public static void saveUser(LoginResponse response, Context context) {
        sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(username, response.getUser().getUsername());
        editor.putString(accessToken, response.getAccessToken());
        editor.apply();
    }

    public static String getAccessToken(Context context) {
        sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        return "Bearer " + sharedPreferences.getString(accessToken, "");
    }

    public static void removeUser(Context context) {
        sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(username);
        editor.remove(accessToken);
        editor.apply();
    }
}
