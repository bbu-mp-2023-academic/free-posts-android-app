package com.dinsaren.free_posts_android_app.ui.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.adapters.SpinnerCategoryAdapter;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.app.BaseActivity;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.models.CategoryResponse;
import com.dinsaren.free_posts_android_app.models.product.FindProductResponse;
import com.dinsaren.free_posts_android_app.models.product.Product;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormProductActivity extends BaseActivity {
    private Spinner spinnerCategory;
    private APIInterface apiInterface;
    private SpinnerCategoryAdapter spinnerCategoryAdapter;
    private EditText productName, productNameKh, price, cost, discount;
    private Category category = new Category();
    private Product product = new Product();
    private RadioButton rbActive, rbDelete;

    private List<Category> categoryList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_product);
        setTitle("Create New Product");
        spinnerCategory = findViewById(R.id.spinnerCategory);
        productName = findViewById(R.id.etProductName);
        productNameKh = findViewById(R.id.etProductNameKh);
        price = findViewById(R.id.etPrice);
        cost = findViewById(R.id.etCost);
        discount = findViewById(R.id.etDiscount);
        rbActive = findViewById(R.id.rbActive);
        rbDelete = findViewById(R.id.rbDelete);
        progressBar = findViewById(R.id.progressBar);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.getAllCategory(UserSharedPreference.getAccessToken(this)).enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body().getData() != null) {
                        categoryList = response.body().getData();
                        spinnerCategoryAdapter = new SpinnerCategoryAdapter(response.body().getData(), FormProductActivity.this);
                        spinnerCategory.setAdapter(spinnerCategoryAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                category = categoryList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getProductById();
    }

    public void onClickCreateProduct(View view) {
        if (productName.getText().toString().equals("")) {
            showErrorMessage("Product is required", view);
            return;
        }
        product.setName(productName.getText().toString().trim());
        product.setNameKh(productNameKh.getText().toString().trim());
        product.setPrice(Double.valueOf(price.getText().toString().trim()));
        product.setCost(Double.valueOf(cost.getText().toString().trim()));
        product.setDiscount(Double.valueOf(discount.getText().toString().trim()));
        product.setImageUrl(null);
        product.setStatus(rbActive.isChecked() ? "ACT" : "DEL");
        product.setCategory(category);

        progressBar.setVisibility(View.VISIBLE);
        apiInterface.createProduct(product, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getProductById() {
        Intent intent = getIntent();
        int id = intent.getIntExtra("ID", 0);
        if (id != 0) {
            progressBar.setVisibility(View.VISIBLE);
            apiInterface.getProductById(id, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<FindProductResponse>() {
                @Override
                public void onResponse(Call<FindProductResponse> call, Response<FindProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            productName.setText(response.body().getData().getName().toString());
                            productNameKh.setText(response.body().getData().getNameKh().toString());
                            price.setText(response.body().getData().getPrice().toString());
                            discount.setText(response.body().getData().getDiscount().toString());
                            cost.setText(response.body().getData().getCost().toString());
                            category = response.body().getData().getCategory();
                            product = response.body().getData();
                            for (int i = 0; i < categoryList.size(); i++) {
                                if (category.getId() == categoryList.get(i).getId()) {
                                    spinnerCategory.setSelection(i);
                                    return;
                                }
                            }
                            if(product.getStatus().equals("ACT")){
                                rbActive.setChecked(true);
                            }else{
                                rbDelete.setChecked(true);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<FindProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }


}