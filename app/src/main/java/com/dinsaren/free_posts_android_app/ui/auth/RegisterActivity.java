package com.dinsaren.free_posts_android_app.ui.auth;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.models.register.RegisterRequest;
import com.dinsaren.free_posts_android_app.models.register.RegisterResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText username, email, phone, firstName, lastName, password, confirmPassword;
    private ProgressBar progressBar;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        setVariable();
    }

    public void initView() {
        username = findViewById(R.id.etUsername);
        email = findViewById(R.id.etEmail);
        phone = findViewById(R.id.etPhoneNumber);
        firstName = findViewById(R.id.etFirstName);
        username = findViewById(R.id.etUsername);
        lastName = findViewById(R.id.etLastName);
        password = findViewById(R.id.etPassword);
        confirmPassword = findViewById(R.id.etConfirmPassword);
        progressBar = findViewById(R.id.progressBar);
    }

    public void setVariable() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void onClickRegister(View view) {
        if (username.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Username is required", Toast.LENGTH_LONG).show();
            return;
        }
        if (phone.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Phone is required", Toast.LENGTH_LONG).show();
            return;
        }
        if (email.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Email is required", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Password is required", Toast.LENGTH_LONG).show();
            return;
        }

        if (password.getText().toString().equals("")) {
            Toast.makeText(RegisterActivity.this, "Confirm password is required", Toast.LENGTH_LONG).show();
            return;
        }

        if (!password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
            Toast.makeText(RegisterActivity.this, "Confirm password is not match", Toast.LENGTH_LONG).show();
            return;
        }
        RegisterRequest req = new RegisterRequest();
        req.setId(0);
        req.setFirstName(firstName.getText().toString().trim());
        req.setLastName(lastName.getText().toString().trim());
        req.setUsername(username.getText().toString().trim());
        req.setPhone(phone.getText().toString().trim());
        req.setEmail(email.getText().toString().trim());
        req.setPassword(password.getText().toString().trim());
        req.setConfirmPassword(confirmPassword.getText().toString().trim());
        try {
            progressBar.setVisibility(View.VISIBLE);
            apiInterface.register(req).enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        showMessage("Register success");
                        finish();
                    }
                    if (response.code() == 400) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            showMessage(jObjError.getString("message"));
                        } catch (JSONException | IOException e) {
                            showMessage("Error Register success : " + e.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    showMessage("Error Register success");
                }
            });
        } catch (Throwable e) {
            showMessage("Error Register success : " + e.getMessage());
        }

    }

    public void showMessage(String message) {
        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_LONG).show();
    }
}