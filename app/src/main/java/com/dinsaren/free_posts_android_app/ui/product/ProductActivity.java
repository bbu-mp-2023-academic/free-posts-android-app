package com.dinsaren.free_posts_android_app.ui.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.adapters.ProductAdapter;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.product.Product;
import com.dinsaren.free_posts_android_app.models.product.ProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity {
    private RecyclerView recyclerViewProduct;
    private ProductAdapter productAdapter;
    private APIInterface apiInterface;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        recyclerViewProduct = findViewById(R.id.recyclerViewProduct);
        progressBar = findViewById(R.id.progressBar);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        setTitle("List Products");
        getData();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                getData();
            }
        });
    }

    private void getData() {
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.getAllProducts(UserSharedPreference.getAccessToken(this)).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    productAdapter = new ProductAdapter(response.body().getData(), ProductActivity.this, new ProductAdapter.OnClickListener() {
                        @Override
                        public void onClickEdit(Product product) {
                            Intent intent = new Intent(ProductActivity.this, FormProductActivity.class);
                            intent.putExtra("ID", product.getId());
                            startActivity(intent);
                        }

                        @Override
                        public void onClickDelete(Product product) {
                            showConfirmDelete(product);
                        }
                    });
                    GridLayoutManager gridLayoutManager = new GridLayoutManager(ProductActivity.this, 1);
                    recyclerViewProduct.setLayoutManager(gridLayoutManager);
                    recyclerViewProduct.setAdapter(productAdapter);
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void onOpenForProductActivity(View view) {
        Intent intent = new Intent(this, FormProductActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }


    private void showConfirmDelete(Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message");
        builder.setMessage("Do you want to delete : " + product.getName());
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressBar.setVisibility(View.VISIBLE);
                apiInterface.deleteProduct(product, UserSharedPreference.getAccessToken(ProductActivity.this)).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        progressBar.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            getData();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}