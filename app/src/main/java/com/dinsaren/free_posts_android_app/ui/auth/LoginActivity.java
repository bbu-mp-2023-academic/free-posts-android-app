package com.dinsaren.free_posts_android_app.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.free_posts_android_app.MainActivity;
import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.login.LoginRequest;
import com.dinsaren.free_posts_android_app.models.login.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText username, password;
    private ProgressBar progressBar;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        initView();
    }

    public void initView() {
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);
        progressBar = findViewById(R.id.progressBar);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void onClickLogin(View view) {
        if (username.getText().toString().equals("")) {
            Toast.makeText(LoginActivity.this, "Username is required", Toast.LENGTH_LONG).show();
            return;
        }
        if (password.getText().toString().equals("")) {
            Toast.makeText(LoginActivity.this, "Password is required", Toast.LENGTH_LONG).show();
            return;
        }
        LoginRequest req = new LoginRequest();
        req.setPhoneNumber(username.getText().toString().trim());
        req.setPassword(password.getText().toString().trim());
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.login(req).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    UserSharedPreference.saveUser(response.body(), LoginActivity.this);
                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_LONG).show();
                    Intent intent  = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                if (response.code() == 401) {
                    Toast.makeText(LoginActivity.this, "Your username or password!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    public void onClickRegister(View view){
        Intent intent  = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}