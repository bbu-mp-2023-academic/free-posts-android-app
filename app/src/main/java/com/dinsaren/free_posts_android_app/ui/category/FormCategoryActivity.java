package com.dinsaren.free_posts_android_app.ui.category;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.app.BaseActivity;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.models.FindCategoryResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormCategoryActivity extends BaseActivity {
    private EditText categoryName, categoryNameKh;
    private Button btnCreate;
    private RadioButton rbActive, rbDelete;

    private APIInterface apiInterface;
    private Category category = new Category();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_category);
        setTitle("Create Category");
        initView();
        findCategoryById();
    }

    private void initView() {
        categoryName = findViewById(R.id.etCategoryName);
        categoryNameKh = findViewById(R.id.etCategoryNameKh);
        rbActive = findViewById(R.id.rbActive);
        rbDelete = findViewById(R.id.rbDelete);
        progressBar = findViewById(R.id.progressBar);
        btnCreate = findViewById(R.id.btnCreate);
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void findCategoryById() {
        Intent intent = getIntent();
        Integer id = intent.getIntExtra("ID", 0);
        if (id != 0) {
            progressBar.setVisibility(View.VISIBLE);

            apiInterface.getCategory(id, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<FindCategoryResponse>() {
                @Override
                public void onResponse(Call<FindCategoryResponse> call, Response<FindCategoryResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            category = response.body().getData();
                            categoryName.setText(category.getName());
                            categoryNameKh.setText(category.getNameKh());
                            if (category.getStatus().equals("ACT")) {
                                rbActive.setChecked(true);
                            } else {
                                rbDelete.setChecked(true);
                            }
                            setTitle("Update Category");
                            btnCreate.setText("Update");
                        }
                    }
                }

                @Override
                public void onFailure(Call<FindCategoryResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    public void onClickCreate(View view) {
        if (categoryName.getText().toString().equals("")) {
            showErrorMessage("Category name is required", view);
            return;
        }

        if (categoryNameKh.getText().toString().equals("")) {
            showErrorMessage("Category name khmer is required", view);
            return;
        }
        category.setName(categoryName.getText().toString().trim());
        category.setNameKh(categoryNameKh.getText().toString().trim());
        category.setImageUrl(null);
        category.setStatus(rbActive.isChecked() ? "ACT" : "DEL");
        //Toast.makeText(this, category.toString(), Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.VISIBLE);
        if (category.getId() != 0) {
            apiInterface.updateCategory(category, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        showErrorMessage("Update Category Success", view);
                        finish();
                    } else {
                        showErrorMessage("Update Category fail", view);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    showErrorMessage("ERROR : " + t.getMessage(), view);
                }
            });
        } else {
            apiInterface.createCategory(category, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        showErrorMessage("Create Category Success", view);
                        finish();
                    } else {
                        showErrorMessage("Create Category fail", view);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    showErrorMessage("ERROR : " + t.getMessage(), view);
                }
            });
        }

    }
}