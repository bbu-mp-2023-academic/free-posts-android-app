package com.dinsaren.free_posts_android_app.ui.category;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.adapters.CategoryAdapter;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.models.CategoryResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity {
    private SwipeRefreshLayout swipeRefreshLayout;
    private CategoryAdapter categoryAdapter;
    private APIInterface apiInterface;
    private ProgressBar progressBar;
    private RecyclerView recyclerViewCategory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        progressBar = findViewById(R.id.progressBar);
        recyclerViewCategory = findViewById(R.id.recyclerViewCategory);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getData();
        swipeRefreshLayout =findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                getData();
            }
        });
    }

    public void getData(){
        progressBar.setVisibility(View.VISIBLE);
        apiInterface.getAllCategory(UserSharedPreference.getAccessToken(this)).enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getData() != null){
                        categoryAdapter = new CategoryAdapter(response.body().getData(), CategoryActivity.this, new CategoryAdapter.OnClickListener() {
                            @Override
                            public void onClickDelete(View view, Category category) {
                                showConfirmDelete(category);
                            }
                        });
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(CategoryActivity.this, 1);
                        recyclerViewCategory.setLayoutManager(gridLayoutManager);
                        recyclerViewCategory.setAdapter(categoryAdapter);

                    }
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    public void onClickOpenFormCategory(View view){
        Intent intent = new Intent(CategoryActivity.this, FormCategoryActivity.class);
        startActivity(intent);
    }

    private void showConfirmDelete(Category category){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Message");
        builder.setMessage("Do you want to delete : "+category.getName());
       builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
               progressBar.setVisibility(View.VISIBLE);
               apiInterface.deleteCategory(category, UserSharedPreference.getAccessToken(CategoryActivity.this)).enqueue(new Callback<Void>() {
                   @Override
                   public void onResponse(Call<Void> call, Response<Void> response) {
                       progressBar.setVisibility(View.GONE);
                       if(response.isSuccessful()){
                           getData();
                       }
                   }

                   @Override
                   public void onFailure(Call<Void> call, Throwable t) {
                       progressBar.setVisibility(View.GONE);
                   }
               });
           }
       });
       builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
               dialogInterface.dismiss();
           }
       });
       AlertDialog alertDialog = builder.create();
       alertDialog.show();
    }
}