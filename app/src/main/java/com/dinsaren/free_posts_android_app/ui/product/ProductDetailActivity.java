package com.dinsaren.free_posts_android_app.ui.product;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.api.APIClient;
import com.dinsaren.free_posts_android_app.api.APIInterface;
import com.dinsaren.free_posts_android_app.data.local.UserSharedPreference;
import com.dinsaren.free_posts_android_app.models.product.FindProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {
    private TextView productName, price, categoryName, discount;
    private APIInterface apiInterface;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        initView();
        setVariable();
        getProductById();
    }

    public void initView() {
        productName = findViewById(R.id.tvProductName);
        price = findViewById(R.id.tvPrice);
        discount = findViewById(R.id.tvDiscount);
        categoryName = findViewById(R.id.tvCategoryName);
        progressBar = findViewById(R.id.progressBar);
    }

    public void setVariable() {
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    public void getProductById() {
        progressBar.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        int id = intent.getIntExtra("ID", 0);
        if (id != 0) {
            apiInterface.getProductById(id, UserSharedPreference.getAccessToken(this)).enqueue(new Callback<FindProductResponse>() {
                @Override
                public void onResponse(Call<FindProductResponse> call, Response<FindProductResponse> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().getData() != null) {
                                productName.setText(response.body().getData().getName());
                                discount.setText(String.valueOf(response.body().getData().getDiscount()) +" %");
                                price.setText("$ "+String.valueOf(response.body().getData().getPrice()));
                                categoryName.setText(response.body().getData().getCategory().getName());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<FindProductResponse> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

}