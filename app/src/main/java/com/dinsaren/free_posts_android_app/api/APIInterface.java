package com.dinsaren.free_posts_android_app.api;

import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.models.CategoryResponse;
import com.dinsaren.free_posts_android_app.models.FindCategoryResponse;
import com.dinsaren.free_posts_android_app.models.home.HomeResponse;
import com.dinsaren.free_posts_android_app.models.login.LoginRequest;
import com.dinsaren.free_posts_android_app.models.login.LoginResponse;
import com.dinsaren.free_posts_android_app.models.product.FindProductResponse;
import com.dinsaren.free_posts_android_app.models.product.Product;
import com.dinsaren.free_posts_android_app.models.product.ProductResponse;
import com.dinsaren.free_posts_android_app.models.register.RegisterRequest;
import com.dinsaren.free_posts_android_app.models.register.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {
    @GET("/api/admin/category/list")
    Call<CategoryResponse> getAllCategory(@Header("Authorization") String authorization);

    @POST("/api/admin/category/create")
    Call<Void> createCategory(@Body Category req, @Header("Authorization") String authorization);

    @GET("/api/admin/category/{id}")
    Call<FindCategoryResponse> getCategory(@Path("id") Integer id, @Header("Authorization") String authorization);

    @POST("/api/admin/category/update")
    Call<Void> updateCategory(@Body Category req, @Header("Authorization") String authorization);

    @POST("/api/admin/category/delete")
    Call<Void> deleteCategory(@Body Category req, @Header("Authorization") String authorization);

    @GET("/api/admin/product/list")
    Call<ProductResponse> getAllProducts(@Header("Authorization") String authorization);

    @POST("/api/admin/product/create")
    Call<Void> createProduct(@Body Product req, @Header("Authorization") String authorization);

    @GET("/api/admin/product/{id}")
    Call<FindProductResponse> getProductById(@Path("id") Integer id, @Header("Authorization") String authorization);

    @POST("/api/admin/product/delete")
    Call<Void> deleteProduct(@Body Product req, @Header("Authorization") String authorization);

    @POST("/api/oauth/token")
    Call<LoginResponse> login(@Body LoginRequest req);

    @POST("/api/oauth/register")
    Call<RegisterResponse> register(@Body RegisterRequest req);


    @GET("/api/admin/category/products/list")
    Call<HomeResponse> getAllCategoryWithProduct(@Header("Authorization") String authorization);


}
