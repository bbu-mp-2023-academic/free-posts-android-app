package com.dinsaren.free_posts_android_app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.ui.category.FormCategoryActivity;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private List<Category> categoryList;
    private Context context;
    private OnClickListener onClickListener;

    public CategoryAdapter(List<Category> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    public CategoryAdapter(List<Category> categoryList, Context context, OnClickListener onClickListener) {
        this.categoryList = categoryList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_card_item_layout, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categoryList.get(position);
        if (category != null) {
            holder.title.setText(category.getName().toString());
            if(category.getStatus().equals("ACT")){
                holder.status.setText("Active");
                holder.status.setTextColor(context.getColor(R.color.purple_500));
            }else{
                holder.status.setText("Delete");
                holder.status.setTextColor(context.getColor(R.color.red));
            }
            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, FormCategoryActivity.class);
                    intent.putExtra("ID", category.getId());
                    context.startActivity(intent);
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClickDelete(view, category);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView image, edit, delete;
        TextView title, status;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);
            title = itemView.findViewById(R.id.title);
            status = itemView.findViewById(R.id.status);
        }
    }

    public interface OnClickListener{
        void onClickDelete(View view, Category category);
    }
}
