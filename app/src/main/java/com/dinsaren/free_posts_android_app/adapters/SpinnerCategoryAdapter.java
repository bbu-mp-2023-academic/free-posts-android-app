package com.dinsaren.free_posts_android_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.models.Category;

import java.util.List;

public class SpinnerCategoryAdapter extends BaseAdapter {
    private List<Category> categoryList;
    private Context context;
    private OnClickListener onClickListener;

    public SpinnerCategoryAdapter(List<Category> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    public SpinnerCategoryAdapter(List<Category> categoryList, Context context, OnClickListener onClickListener) {
        this.categoryList = categoryList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return categoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View currentView = view;
        Category category = categoryList.get(i);
        if (currentView == null) {
            currentView = LayoutInflater.from(context).inflate(R.layout.spinner_category_card_item_layout, null);
            CategoryViewHolder categoryViewHolder = new CategoryViewHolder();
            categoryViewHolder.title = currentView.findViewById(R.id.title);
            if (category != null) {
                categoryViewHolder.title.setText(category.getName().toString());
            }

        }
        return currentView;
    }


    public static class CategoryViewHolder {
        TextView title;
    }

    public interface OnClickListener {
        void onClickDelete(View view, Category category);
    }
}
