package com.dinsaren.free_posts_android_app.adapters.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.models.Category;
import com.dinsaren.free_posts_android_app.models.product.Product;

import java.util.List;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.CategoryViewHolder> {
    private List<Category> categories;
    private Context context;
    private OnClickListener onClickListener;

    public HomeCategoryAdapter(List<Category> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    public HomeCategoryAdapter(List<Category> categories, Context context, OnClickListener onClickListener) {
        this.categories = categories;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_card_item_product_layout, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        if (category != null) {
            holder.tvTitle.setText(category.getName());
            if (category.getProducts() != null) {
                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1, RecyclerView.HORIZONTAL, false);
                HomeProductAdapter productAdapter = new HomeProductAdapter(category.getProducts(), context, new HomeProductAdapter.OnClickListener() {
                    @Override
                    public void onClickProductImage(Product product) {
                        onClickListener.onClickProductImage(product);
                    }
                });
                holder.recyclerViewProduct.setLayoutManager(gridLayoutManager);
                holder.recyclerViewProduct.setAdapter(productAdapter);

            }
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvViewMore;
        RecyclerView recyclerViewProduct;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvViewMore = itemView.findViewById(R.id.tvViewMores);
            recyclerViewProduct = itemView.findViewById(R.id.recyclerViewProduct);
        }
    }

    public interface OnClickListener{
        void onClickProductImage(Product product);
    }
}
