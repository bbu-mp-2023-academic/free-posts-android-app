package com.dinsaren.free_posts_android_app.adapters.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.models.product.Product;

import java.util.List;

public class HomeProductAdapter extends RecyclerView.Adapter<HomeProductAdapter.ProductViewHolder> {

    private List<Product> products;
    private Context context;
    private OnClickListener onClickListener;

    public HomeProductAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }

    public HomeProductAdapter(List<Product> products, Context context, OnClickListener onClickListener) {
        this.products = products;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_card_item_layout, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = products.get(position);
        if (product != null) {
            holder.productName.setText(product.getName());
            holder.price.setText("$ " + product.getPrice().toString());
            holder.discount.setText(product.getDiscount().toString() + " %");
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClickProductImage(product);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView productName, price, discount;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivImage);
            productName = itemView.findViewById(R.id.tvProductName);
            price = itemView.findViewById(R.id.tvPrice);
            discount = itemView.findViewById(R.id.tvDiscount);
        }
    }

    public interface OnClickListener{
        void onClickProductImage(Product product);
    }

}
