package com.dinsaren.free_posts_android_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinsaren.free_posts_android_app.R;
import com.dinsaren.free_posts_android_app.models.product.Product;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private List<Product> productList;
    private Context context;
    private OnClickListener onClickListener;

    public ProductAdapter(List<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    public ProductAdapter(List<Product> productList, Context context, OnClickListener onClickListener) {
        this.productList = productList;
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_list_item_layout, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = productList.get(position);
        if(product.getName() !=null){
            holder.productName.setText(product.getName());
        }

        if(product.getCategory() !=null){
            holder.categoryName.setText(product.getCategory().getName());
        }

        if(product.getStatus() !=null && product.getStatus().equals("ACT")){
            holder.status.setText("Active");
            holder.status.setTextColor(context.getColor(R.color.purple_500));
        }
        if(product.getStatus() !=null && product.getStatus().equals("DEL")){
            holder.status.setText("Delete");
            holder.status.setTextColor(context.getColor(R.color.red));
        }
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClickEdit(product);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClickDelete(product);
            }
        });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder  extends RecyclerView.ViewHolder{
        ImageView productImage, edit, delete;
        TextView productName, categoryName, status;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            productImage = itemView.findViewById(R.id.ivImage);
            edit = itemView.findViewById(R.id.ivEdit);
            productName = itemView.findViewById(R.id.tvProductName);
            categoryName = itemView.findViewById(R.id.tvCategoryName);
            status = itemView.findViewById(R.id.tvStatus);
            delete = itemView.findViewById(R.id.ivDelete);

        }
    }

    public interface OnClickListener{
        void onClickEdit(Product product);
        void onClickDelete(Product product);
    }
}
